# Glossary

There's plenty of jargon that goes around which may overwhelm a newcomer. See [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) for any terms that may not be listed here.

If you feel there's a piece of lingo that isn't covered by [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) nor by this document, let us know!

**Apros**: A custom LMS front-end, used by one of our clients (Yonkers). It replaces the standard Open edX LMS front-end.

**Epic**: A big chunk of work that has one common objective. It could be a feature, customer request or business requirement. These would be difficult to estimate or to complete in a single iteration. Epics contain smaller, broken down tasks meant for iterative completion.

**Discovery**: The process of "discovering" the set of tasks, and the approximate [time estimations](how_to_do_estimates.md) & level of effort needed for the tasks of an Epic.

**Firefighter**: A firefighter is a sprint's facilitator. The firefighter's responsibilities include handling emergencies, attempting to unblock people, watching over potential spillover, and more. See [the corresponding JIRA ticket](https://tasks.opencraft.com/browse/OC-1199).

**Points**: The approximate "level of effort" and time a task would require. See [the OpenCraft Process](process.md).

**Timebox**: A cap on time we plan on spending for a task -- going over the timebox is not permitted. If it looks like there is a risk of going over the timebox, ping Braden and/or the epic owner to discuss.

**Ocim**: Short for "OpenCraft Instance Manager". It's our in-house (but open source) continuous integration project that watches for our PRs against the `edx/edx-platform` repository and automatically spins up a sandbox that contains the PR's version of the platform. See [how to spin up sandboxes](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/sandboxes.md).

**OVH**: The OpenStack-based cloud computing service we use to host OCIM VMs and more. See [their homepage](https://www.ovh.co.uk/).

**OSPR**: Short for "Open Source Pull Request". This is the edX process for reviewing pull requests from the open source community.

**IDA**: Short for "Independently Deployable Application".

**Code drift**: This refers to changes in forks that aren't present in upstream. Code drift must be maintained (ported, tested, sometimes rewritten) across new release versions, and so adds to our maintenance costs. OpenCraft aims to minimize code drift by upstreaming as much as possible and consolidating the rest into common branches that are shared among several client sites.

TODO: Add more lingo and upstream if needed to [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html).
