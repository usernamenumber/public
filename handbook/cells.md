# Cells

This is the page on which the different cells within OpenCraft are described, and where cell-specific
rules are documented.

## Serenity

Internal tools:

* [Chat](https://chat.opencraft.com/opencraft/channels/serenity)
* [Weekly sprint board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=24&view=planning)
* [Epics board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=27)

Members:

* Adolfo R. Brandes
* Daniel Clemente Laboreo
* Geoffrey Lehée
* Jill Vogel
* Kahlil Hodgson
* Matjaz Gregoric
* Pooja Kulkarni
* Samuel Walladge
* Tim Krones
* Usman Khalid

Roles:

* [Review & mentoring of candidate hires](roles.md#cell-manager-recruitment): Usman Khalid
* [Sprint management](roles.md#cell-manager-sprint-management): Matjaz Gregoric
* [Epic planning](roles.md#cell-manager-epic-planning): Tim Krones
* [Sustainability of the cell](roles.md#cell-manager-sustainability-of-the-cell): Daniel Clemente Laboreo
* [Prioritization of work](roles.md#cell-manager-prioritization-of-work): Jill Vogel
* [OSPR Liaison](roles.md#ospr-liaison): Jill Vogel
* [Official forum moderator](roles.md#official-forum-moderator): Samuel Walladge
* [DevOps Specialist](roles.md#devops-specialist): Adolfo R. Brandes

Cell-specific rules:

* None

## Bebop

Internal tools:

* [Chat](https://chat.opencraft.com/opencraft/channels/bebop)
* [Weekly sprint board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=26&view=planning)
* [Epics board](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=28)

Members:

* Giovanni Cimolin da Silva
* Guruprasad Lakshmi Narayanan
* Josh McLaughlin
* Kshitij Sobti
* Paulo Viadanna
* Piotr Surowiec
* Romina Killpack
* Rushal Verma

Roles:

* [Review & mentoring of candidate hires](roles.md#cell-manager-recruitment): Paulo Viadanna
* [Sprint management](roles.md#cell-manager-sprint-management): Giovanni Cimolin da Silva
* [Epic planning](roles.md#cell-manager-epic-planning): Kshitij Sobti
* [Sustainability of the cell](roles.md#cell-manager-sustainability-of-the-cell): Guruprasad Lakshmi Narayanan
* [OSPR Liaison](roles.md#ospr-liaison): Giovanni Cimolin da Silva
* [Official forum moderator](roles.md#official-forum-moderator): Paulo Viadanna
* [DevOps Specialist](roles.md#devops-specialist): Giovanni Cimolin da Silva

Cell-specific rules:

* None

## Outside of cells

* Braden MacDonald
* Gabriel D'Amours
* Xavier Antoviaque

Roles:

* [Official forum moderator](roles.md#official-forum-moderator): Xavier Antoviaque
