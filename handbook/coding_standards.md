# OpenCraft Coding Standards & Guidelines

Here are the guidelines that OpenCraft follows with all of our software development. Some of these are not strict rules, but all of them are the standards that we aim for.

## Open Source

We are an open source company which means that all software we create should be open source by default (unless there is some compelling or legal reason not to).

New software that we create should generally use the [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html) or [GPL](https://www.gnu.org/licenses/gpl-3.0.html) license.

Documentation or other non-software works that we create should be licensed under a Creative Commons license, usually [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).

In addition, any time we make a change (feature, bugfix, etc.) to an external software project (like Open edX, Django, etc.), we contribute that change back to the project. (Sometimes they won't accept it as a contribution, but we always try.)

## Coding Standards

We write most of our code in Python and HTML/JavaScript/CSS, though we use a little bit of many other languages and technologies (e.g. shell scripts, ansible, terraform, groovy, SQL). We default to using Python for most new projects for simplicity and consistency.

General guidelines:

* Do your best to ensure you've written **clean, maintainable code**
  * "Don't make me think!": use verbose names in your code to make it obvious what the code is doing to anyone who reads it. When it's not obvious due to naming alone, add comments and docstrings to make it obvious.
* Write tests for your code
  * Good test coverage doesn't necessarily mean perfect branch coverage, but instead means code is covered proportionally to how much it will be used.

Python guidelines:

* New python projects should be writting using Python 3.5 or newer
* Any new python code we write in python 2.7 (e.g. XBlocks, or contributions to edx-platform) must be forwards-compatible with Python 3 (use `six`, etc.)
* Follow the [edX Python Style Guide](http://edx.readthedocs.io/projects/edx-developer-guide/en/latest/style_guides/python-guidelines.html) (even for non-edX projects, unless the project has its own style guide).
* Use pycodestyle and pylint to enforce coding standards automatically

Frontend guidelines:

* Follow the [edX JavaScript Style Guide](http://edx.readthedocs.io/projects/edx-developer-guide/en/latest/style_guides/javascript-guidelines.html) for JavaScript in edX projects.
* Use TypeScript or plain ES 6 for non-edX projects.
* Ensure that the UI you build follows best practices for accessibility and complies with the WCAG 2.0 Level AA standards:
  * Every UI component and content item has a text version (alt text, screenreader text), so that non-visual users can still understand and use it.
  * The UI can be used with a keyboard only (tab order, keyboard controls).
  * Text colors have [sufficient contrast](https://webaim.org/resources/contrastchecker/).
  * See the [complete checklist/cheatsheet](https://www.wuhcag.com/wcag-checklist/) (levels A and AA only).

More in-depth guidelines can be found in our [technical documentation handbook](https://doc.opencraft.com/en/latest/coding-best-practices).

## Contributing Features to edx-platform

For feature contributions to [the Open edX core platform](https://github.com/edx/edx-platform/) in particular:

Discuss with edX and/or internally to determine if the feature you're building should be part of the core platform or should be a plugin (like an XBlock or a [django app plugin](https://github.com/edx/edx-platform/blob/master/openedx/core/djangoapps/plugins/README.rst)). Generally, if it provides a platform feature that most of the community will use or provides a foundation for building other functionality, it would be a good candidate for the core platform, and if it's a feature that only a minority of Open edX instances are likely to use, it would be better as a plugin.

If you are writing the feature as a plugin, ensure that it only uses stable public APIs such as the [XBlock API](https://openedx.atlassian.net/wiki/spaces/AC/pages/161400730/Open+edX+Runtime+XBlock+API), the [Django App Plugin API](https://github.com/edx/edx-platform/blob/master/docs/decisions/0002-inter-app-apis.rst), and/or any [inter-app API](https://github.com/edx/edx-platform/blob/master/docs/decisions/0002-inter-app-apis.rst) (`api.py` files within each Django app in the core platform). **If there is no public API defined** for the integration you need, modify the core platform to provide the API that you need, e.g. [in an `api.py` file](https://github.com/edx/edx-platform/blob/master/docs/decisions/0002-inter-app-apis.rst), and contribute the API to the core platform.
