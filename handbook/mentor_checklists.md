# Initial 121

This meeting objective is to get the mentor to know better about the newcomer capabilities and help in the screening process. Here are some example questions to help with that:

* Are you comfortable working in a POSIX environment?
* Do you have experience coding in Python? How many years?
* What is your skill level in frontend development? Do you have experience with any frameworks?
* How do you feel working 100% remotely? Is this your first experience?

# Weekly 121s

These meetings will be used mostly for answer questions, both technical or related to OpenCraft processes.

* Evaluate the newcomer's current sprint and tasks currently at risk of spilling over.
At-risk tasks should be flagged with the sprint firefighter, to avoid missing delivery deadlines.
* Help the newcomer prioritize work and check if there are any difficulties working remotely.
* Provide feedback, both on technical and other skills such as communication, time management etc. This feedback may come from other team members, or from my observations on the newcomer's assigned tasks.
* Help the newcomer use accurate task statuses during their sprints to ensure the Sprint Commitments spreadsheet for
their cell is accurate when it comes time to do the developer review. If there are any issues with what is recorded
there, let the [Sprint manager for your cell](cells.md) know.