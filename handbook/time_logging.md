# Time logging best practices

Logging time is required of every team member and can be
challenging. The granularity of the tasks is surprisingly fine to the
newcomer and even seasoned core members can be helped by the best
practices listed here. The spirit is that all work should be logged:

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> [...] ask yourself first is “should I spend X amount of time on
> this thing”; once you believe the answer is yes, then there’s no
> question that you should log the time because we don’t want people
> doing work they’re not getting paid for.

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/2):

> [...] there should not be any time that isn’t loggable. If you have
> to rework an upstream PR, that should go on the ticket from that PR,
> even answering emails or answering in the forums should go on a
> ticket (if there is no ticket for it, ask!). It might sometimes
> creates issue with budgets, but if you don’t log the time at all
> that will just hide the problem, and you will end up being the one
> who loses quietly - while if there is a budget issue, we’ll look at
> it together.

## Testimonies

Randomly organized testimonies related to time logging which are
either inspiring or funny.

### Brandon

I partially thought [worklogs] were in place to specifically be used
against me. It took me some time to fully come to terms with the
hourly stuff. Most of the guilt/frustration went away the more I:

* Trusted this stuff wasn't being used against me.

* Saw that other people on the team struggling with tech stuff and
  spillover like I was - I think it's important to figure out what
  "normal" really is on a team. It's not usually the superhuman thing
  one thinks it is initially.

At the end of the day, look through your activity history and sent
emails and make sure you didn't miss logging anything. When you're
bouncing back and forth between things rapidly, or there's a lot of
different issues grabbing your attention, it's really easy to miss
logging your time. I find that towards the end of particularly hectic
days, rather than feeling accomplished, I'm left feeling unsettled
about my time logs. It can be worth taking a quick look through your
activity history (even if you probably logged everything), just to
kick that feeling so you can start your work the next day more
refreshed.

### Jill

I docked my hours when I felt like my struggles were the fault of my
own incompetence, and resented the job for "making" me do it. I viewed
the OpenCraft team members as superhuman — the amount of work you guys
get done, and how quickly and well you do it? Freaking amazing. The
level of scrutiny performed on even the smallest ticket, and the
quality of the code we produce, is incredible. I've never even had a
job where my code was reviewed, let alone criticised line by line, and
definitely not on public, open-source websites. Initially, when I
started this job, I'd have to take a deep breath before replying with
a "thank you for spotting that!", but now, I love seeing how clean the
resulting code is once it's been through our wringer. I'm proud of
what I create, and prouder now when it survives the reviews. But that
took time.

## Tips that help with time logging

These tips are related to recuring tasks or situations that require
time tracking and suggest how they can be approached. It is not a
policy and it is up to each team member to decide to apply them or
not.

### Handling help@

For most people on the team, you don’t have to read the vast majority
of the messages on help@. It’s fine to ignore it almost completely,
and to only occasionally scan for unanswered threads or subjects which
might involve you. You could configure mail filters so help@ messages
never even arrive in your inbox unless you're explicitly CC’d or you
are watching the thread.

### Checking normal emails with filters

You can have filters which automatically label each message with the
relevant client, and then it’s pretty easy to read all the emails from
one client at a time (e.g. I read all the green “Yonkers” emails in
various threads for 10min then log 10min on the “Yonkers Support”
epic; then I read all the blue Sophrogal emails for 15 min and log
that 15min on whatever Sophrogal epic the majority of those emails
were about; and so on…)

### Checking incidents as they happen

We have a [specific ticket for that](https://tasks.opencraft.com/browse/SE-114)
or the relevant instance maintenance ticket if just one instance is offline.

### Deciding whether to start a new discussion about XYZ

Deciding whether to start a new discussion about infrastructure,
reflecting about the work, learning about our infrastructure, reading
other tickets, technical problems during work which aren’t actually
related to any task etc. With all of these things, if it’s, say, much
more than 15 minutes and isn’t really related to anything you're
working on, you could just create a new ticket for yourself about
whatever it is and log the time on that.

## What should I log and how?

### Logging more things

It is easy to overlook the time spent preparing the next sprint,
checking the timesheet, requesting for help in chat, reading e-mails,
posting in the forum etc. However, it’s still real work.

If you go a little over a timeboxed ticket, you should log it
instead of hiding it. You'll learn to stop earlier next time so that
you don't go over the timebox, that's better than getting used to
going over the timebox every time.

The exception is invoicing: things related to your own company's
functioning isn't something OpenCraft normally pays for (like the time
you spend doing your own company's accounting or taxes for example).

### Look for recurring tickets

Some tickets are meant to be used to log recurring tasks such as the time
spent in sprint plannings meetings. When in doubt and before asking
for a specific ticket, go to the `Recurring` column of the sprint
dashboard and look for a ticket that may fit what you are doing.

### Start to track your time immediately, before knowing where to assign it

When you sit down to work for OpenCraft, start to log immediately. The
time spent thinking about what you're actually going to work on,
preparing your environment by spawning a devstack, etc. is part of the work
(sometime called metawork). It also involves going through mail,
chat and forums to check in case something relevant came up. Whatever
ticket you start working first is where you should log this time.

### Start to track your time right after completing a task

Unless you're done for the day, you start thinking about the next task
after finishing another. This context switching (or metawork) time
also involves participating in various communication channels and will
be included in the time logged for the next task.

### Split work spread over many tickets after doing it

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/5):

> For work spread over many tickets, like during sprint planning, or
> reading tickets updates/emails, the best is to do this in one chunk,
> time that, and roughly divide the time between the main epics/topics
> that took the most time.

Another use case is if your preference is to get to zero inbox (forum,
chat etc.) once a day, it is likely that you batch the time spent
reaching that goal once a day and that it relates to a number of
tasks.

When you're done with this batch activity (processing mails, sprint
preparation etc.), take a step back and evenly divide the time spent
among all related tickets. For instance:

> You spend 90 minutes going through all mail, forum and chat. When
> you're done, you open the list of issues assigned to you or for which
> you are a reviewer. You pick 6 issues that you came across during
> these 90 minutes and log 15 minutes in each of them.

### Do not log breaks that are real breaks

If you're working on a ticket for an extended period, it is healthy to
get up and move about a bit; something like 50 minutes working and
then 10 minutes break for instance. If you keep thinking about your
work, you should log this time. This applies even if you take those 10
minutes to do something unrelated, like [hang out some
laundry](https://forum.opencraft.com/t/reducing-stress/272/15) - when
the mental fog descends, those 10 minutes to step back are worth more
than an hour of going in circles and not progressing.

For breaks longer than 15 minutes spent doing something entirely
unrelated to work, like discussing with a friend, you should not log
that time because you're not working.

If you were at an office, this time would be paid regardless. This is
one of the reasons why a 9-5 working day at OpenCraft does not amount
to 8 hours of work being logged.

### Don't worry about logging less than 15 minutes in the wrong ticket

When metawork (getting ready to work in the morning, context
switching, reading mail, fighting against a technical problem
preventing you from working, etc.) amounts to less than 15 minutes, it
is OK to log this time to any ticket you end up working on eventually.

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> [...] say I have just finished two hours of coding and I’m about to
> move on to my next task. I stop the timer in Toggl/Tempo and log the
> time I just spent, then start a new timer. Then I’ll [read a couple
> emails / forum posts / reply on Mattermost] and then start my next
> task. That may add 1-3 minutes to the new task, but that’s not a big
> deal, and it all averages out. However, if one of those emails
> balloons into something bigger and I end up spending twenty minutes
> on it instead of one minute, then I’ll just log that time to
> whatever ticket(s) is/are most relevant to that email(s), and start
> over.

[Quoting Xavier Antoviaque](https://forum.opencraft.com/t/time-logging-practices/295/5):

> [...] it’s not completely fair for the ticket you are taking up
> since half of that time is technically from the previous ticket, but
> everything doesn’t need to be precise to the minute, it will average
> out over time.

### When a task longer than 15 minutes don't fit anywhere, create a ticket

It is not uncommon for a discussion to develop and require time to
develop further. When you sense it is likely to be the case, create a
ticket to log the time spent reading, thinking and writing on that
topic. This `Time logging best practices` page, for instance, is the
outcome of a discussion that was associated with a ticket where team
members logged their time on that topic.

[Quoting Braden MacDonald](https://forum.opencraft.com/t/time-logging-practices/295/7):

> “deciding whether to start a new discussion about infrastructure,
>  reflecting about the work, learning about our infrastructure,
>  reading other tickets, technical problems during work which aren’t
>  actually related to any task”: with all of these things [...] if
>  it’s [...] much more than 15 minutes [...], I would just create a
>  new ticket for myself about whatever it was and log the time on
>  that.

### Avoid/reduce some of the activities which are hard to log

Helping with tasks when you are neither assigned to nor a reviewer when
not asked for is generous but it consumes part of the budget that
other participants will probably need later on.
