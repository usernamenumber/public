# Roles

One other important way that we do differently than a lot of companies is the way we assign
responsibility. Instead of titles, which are linked to the concept of hierarchies and ranking,
we use roles. Roles can be taken on by willing people, and people will usually take on multiple
roles, changing over time, based on interest and availability. This allows for a much smoother
progression of responsibilities than the ladder climbing game.

Note that responsibility is also uncorrelated with compensation and raises, which are given the
whole core team at once, based on time spent working at the company and overall financial results.
Compensation isn't a good source of motivation beyond a certain level, and this approach removes
a lot of politics.

If there is any role you would like to take up, the best way is to say it publicly - for example
in the forum. Then when opportunities arise others will remember it, and point you to tickets
you can own. Keep in mind that role assignments are intended to be permanent, so keep that in
mind before picking a role. Ideally you should work in a role for at least one year before you
consider swapping/dropping it.


## Changing Roles

Once someone takes up a role in a call there generally no need to reassign it unless someone leaves
OpenCraft. While a appointement to a role is generally permanent, no one should feel stuck in a
role if they are no longer comfortable in it. As such it is desirable to openly discuss with other
members of the cell on the forum, and perhaps in the sprint meeting when such a situation arises.

It is the responsibility of the current person with a role to find a replacement in their cell and
help their replacement during a transition period while they are still getting comfortable with
their new responsibilities.

Note that for roles like Client Owner or DevOps Specialist where the role involves a fair bit of
specialised knowledge or context, great care should be taken to find a suitable replacement with
a similar level of knowledge and context.

## Types of OpenCraft members

There are three types of members at OpenCraft:

* *Additional members*:
    * *Short-term members* (temporary contractors), who have been hired for a specific task, scope
      or period of time. They are the most external members of the team.
    * *[Newcomers](#newcomer) on probation* (2 months, renewable).
* *Core team member* - the new recruits who have been confirmed become core team members. They differ
  from the other types of members in that they tend to have more team-based responsibilities.
  For example, core team members are on a
  [weekly rotation schedule](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit#gid=447257869)
  where they often have to take on some additional roles, including Sprint Firefighter, being on
  Discovery Duty, and occasionally leading our weekly sprint kick-off meeting.

However, in all other regards, all types of developers are put to the same expectations -- no
politics or special treatment between short-term developers, newcomers, and core team members.

The only acceptable exception is when providing extra mentoring on tasks for newcomers (or anyone
known to not have much context in the task), which is expected and useful.

# What is expected

## From everyone

The general expectations for anyone working at OpenCraft:

### General

* I can work from wherever I want, provided I have access to a reliable high-bandwidth internet
  connection good enough for video calls.
* I can set my own work schedule, and work when I want during the week, as long as **I remain
  available to answer to others** (please see: the **Communication** section below)
* (In general) **I am **not** expected to work weekends** (unless I'm behind on my commitments),
  which should remain an exception, not the rule! If you find yourself forced to work on week ends
  more than once per month, that likely reflects an issue with your time management that needs
  fixing. If I work week ends as a personal choice, I will not expect other team members to also
  work week ends.
* I will make sure my [cell's responsibilities](#cell-member) are continuously being properly
  taken care of, by reviewing their status at least once per week.
* I will work at least the amount of hours per week that is specified in my contract (e.g. 40
  hours/week), averaged over each month, excluding scheduled holiday time.
* I will ensure that clients and people on the cell that I'll be working with (e.g. code reviews)
  know my availability. (e.g. If I'm taking Friday off, I'll make sure people who need me
  to do code review know that).
* When taking time off, I will follow the procedure described on the [vacation's section](vacation.md).
* If I'm unexpectedly sick or unavailable due to an emergency, I'll make every effort to notify
  my cell and ask the sprint firefighters to take over my duties.
* I will provide my own hardware (laptop).
* I will keep my computer(s) secure and up to date as described in the
  [security policy](security_policy.md).
* I will record all hours that I spend on a task using the Tempo timesheets tool in JIRA
  (including things like setting up devstacks and time spent learning, which we all need to do
  and is an important part of the task, especially at the beginning, or when joining a project).
* If I use another tool for tracking my time,
  I will update the JIRA Tempo timesheets within 24 hours (excluding weekend days).
* I will attend the Open edX Conference every year, unless exceptional and important personal
  circumstances prevent me from being present (Note that team members who joined OpenCraft before
  July 31st 2018 are [only encouraged to attend the conference, but not strictly required to](https://forum.opencraft.com/t/open-edx-conference-attendance/134/20)).
* As a conference attendee, I will submit a talk proposal and present it. If my talk isn't
  accepted, I will co-present a talk with someone else from OpenCraft.

### Communication

* **If any client email is addressed to me, I will respond to it within 24 hours (excluding weekend
  days), or ensure that someone else on my cell does.**
    * I will CC or forward to help@opencraft.com all client emails and replies, so that others can take over the thread
      with that client if needed, or refer to it for context.
    * If I can't answer immediately, I will at least send a quick reply to let them know we're
      working on a response, and when they should expect it.
* **I will reply to pings/emails from other OpenCraft team members within 24 hours**
  (again excluding weekend days and scheduled time off).
    * But this should be a "worst case" scenario - completing the work on time
      is still the primary goal, so when someone is blocked and pings me for help,
      I will try to do as much as I can to unblock them quickly,
      rather than starting a 24h ping-pong cycle that takes up all the time
      in the sprint without accomplishing any work.
* **I will respond to pings on GitHub or GitLab within 24 hours**
  (with the usual exclusion for weekend days and scheduled time off).
    * If a ping has no corresponding ticket, or the ticket is not scheduled for
      the current sprint, I will respond to the ping with an estimate for when
      they can expect a full response.
* I will read and respond to forum threads:
    * On the [announcements forum](https://forum.opencraft.com/c/announcements/7): within 24h, excluding week-ends & time off;
    * On the rest of the forums, within 2 weeks, except for the [off topic forum](https://forum.opencraft.com/c/off-topic/8), which doesn't need to be read or replied to at all.
* I will be professional and polite when communicating with clients.
* I will prefer asynchronous over synchronous processes (keep meetings to a minimum). A chat
  conversation is a form of a meeting.
    * Generally, discussions should happen first asynchronously on the JIRA tickets;
      if there is really something that can't be efficiently sorted out asynchronously,
      have a chat or schedule a meeting. JIRA might have long response cycles (around
      1 day turnaround). If this time isn't enough to unblock someone and
      finish their sprint commitments, use Mattermost, even though
      it's more disruptive to people's workflow.
    * If you do have a synchronous conversation with someone about a particular task,
      post a summary of the result/decision from that conversation on the JIRA ticket
      for easier future reference.
    * When scheduling meetings, give them a precise agenda. For people using Calendly,
      like Braden and Xavier, book meetings there, as it allows us to avoid
      the scheduling overhead.
    * Try, as much as possible, to use a similar approach with clients - don't
      let them invade your days with meetings. Calendly is good for this too,
      as it allows to define time slots where you'll have the meetings,
      to minimize the disruption they cause to your day and productivity.
      If you want a calendly account, let Xavier know and he will set
      you up with the OpenCraft account.
* I will post on public channels on Mattermost rather than private 1-to-1 channels
  whenever possible, even if the message is just for one person.
  This allows us to know what others are working on, and helps to replace
  the overheard discussions in physical offices - it can also be an occasion
  for someone else with knowledge about your issue to get the context,
  and to intervene if it is useful to the conversation.
* I will make sure I communicate with my reviewer(s) on tasks about availability
  and timezone overlap if I didn't have knowledge about it before. I will use the
  [contact sheet](https://docs.google.com/spreadsheets/d/107dR9H1vWjLpJlXPuBaFJIFaEPEmaSh50xLow_aEGVw/edit)
  where necessary.
* I will join the weekly sprint meeting of my cell on Mondays, unless I have scheduled that day
  off in advance.

### Sprint tasks

* I will follow the process and expectations outlined in the [pull request process](pull_request_process.md)
  section.
* I will never add my code (even DevOps code!) to a production branch directly - I will always
  create a Pull Request.
* I will always ensure my Pull Requests are reviewed by another OpenCraft team member **before** merging,
  except if I am a *core team member* and I'm merging *trivial changes*.
  In this exceptional case, I may merge the *trivial changes* before receiving a review,
  but I will then ensure all of those *trivial changes* are reviewed and acknowledged post-merge or
  post-deployment by another OpenCraft team member. Trivial changes include:
    * Small Open edX environment tweaks ([example](https://user-images.githubusercontent.com/514483/34581710-c167d10c-f191-11e7-9a66-7fa8c59def82.png))
    * Minor/bugfix package version bumps ([example](https://github.com/open-craft/ansible-rabbitmq/pull/4/files))
* I will only commit to work each sprint that I believe I can comfortably complete within the
  allotted sprint time (two weeks). Here, "complete" means "get to external review or get merged, deployed, and delivered."
* As a core team member, I will avoid taking [newcomer-friendly tasks](task_workflows.md#newcomer-friendly-tasks) unless they are urgent
  and there are no newcomers available to take them on.  I will take on reviews of newcomer-friendly tasks, and allow
  time to provide mentoring and extra guidance.
* I will get all of my tasks ready for internal review (by someone else on the OpenCraft team) by the
  **end of Wednesday in the second week of the sprint** at the latest. This will ensure that
  there is time for the review to take place, and for me to address the review comments
  and get the internal reviewer's approval in time.  If a ticket potentially requires multiple
  review cycles, get it into review as early as possible.  Schedule reviews with your reviewer
  to make sure they have time when you get your work ready for review.
* I will spend time to plan each task at the beginning of each sprint, including scheduling the
  review with the reviewer. Make sure you have an answer for:
    1. When do each step of the task need to be completed?
    1. Which days will you work on which task?
    1. When do individual parts need to be completed to be on time?
* It's also important to keep some margin on the sprint, in case something doesn't go as expected.
* I will get my tasks to at least **External Review**  (or "Deployed & Delivered" if no external review is required) by
  **one hour before the sprint planning meeting for the next sprint**. As this is also dependent on
  the internal reviewer, I'll make sure she/he will be available around the time I finish.
* If it is looking like I will have trouble meeting one of these deadlines,
  I will inform the sprint firefighters (and epic owner) as early as possible,
  so they can get me additional help, start planning for contingencies,
  and/or warn the client.
* If necessary, I will work overtime to complete my tasks before the end of the sprint.
* I will prioritise stories that spilled over from the previous sprint.
* I will "work from the right", prioritizing responding to external reviews
  as the highest priority, then doing reviews / responding to questions
  from others on the team, and finally working on implementing my own "In Progress" tasks.
* I will be helpful to team members, responding to questions,
  helping with debugging, providing advice, or even taking over tasks
  if I have time and they are overloaded. This has precedence over starting
  to work on new tasks, when finishing a sprint early.
* Once a sprint, I will review all of my tasks that are in "Long External Review/Blocked"
  and if needed, I will ping the external reviewer/blocker or pull the task into
  an upcoming sprint.

## Cell member

The coordination on each of these responsibilities is to be assigned, each one to an individual
member of the cell as a recurring task. However, the way this responsibility is handled beyond
what is described in this handbook is the responsibility of the cell as a whole, with all of its
members being considered as weekly reviewers.

Each responsibility can be handled through multiple tasks assigned to multiple owners, as
long as the overall responsibility for the coordination of each item is assigned to a single
person:

### Cell manager - Recruitment

The recruitment manager role is responsible for organizing the selection of candidate hires to match the needs of the [epic planning spreadsheet] for the cell, as determined by the epic planning manager. This includes:
* Creating the newcomer onboarding epic and tasks;
* Finding a mentor for the newcomer, ahead of their start date;
* Organizing the review and confirmation of core developers:
** Ensuring that all core members of the cell participate in the review, as well as one core member from each other cell.
** Compiling the feedback from individual reviews in a single non-nominative list.

### Cell manager - Sprint management

* Each cell has its own sprint board and tickets, and is responsible for handling the sprint,
  as well as its preparation and grooming.
* Every Monday before the sprint planning meeting, review the sprint planning:
    * Tasks from the previous sprint, waiting for 3rd party, and in the backlog;
    * [Epics](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=8&useStoredSettings=true);
    * Rotations - double-check if any of the people assigned to a rotation in the upcoming
      sprint are off, and if so that a backup will be available (eg. that the second firefighter
      will be present, or that a third firefighter has been assigned on those days).
* Assigning the rotations. How to assign the rotation hours within the cell are up to that cell,
  as long as the number of hours matches a % of the total hours of work in the cell. The hours
  can be adjusted slightly if more or less firefighting is required:
    * Firefighting budget: 7.5% (30h/week for 10 full times, split between two firefighters each sprint)
    * Discovery budget: 2.5% (10h/week for 10 full times) - The discovery budget and the discovery duty allocation are two different things. The weekly discovery duty allocation (5h/cell/week) is to be used for new tasks that pop-up in the course of a sprint - it is funded by the discovery budget. Any leftover discovery budget can be used for discovery tasks that are planned in advance.
* Handling the delivery to the client and the verification of the tasks by the clients.
* Update the [sprint commitments spreadsheet](https://docs.google.com/spreadsheets/d/1FXjV9GCugQbX9gS7at2LDDAYvw8A8Z_PrRBG0DaFOho/edit) for your cell during the sprint planning meeting, every week, using the [Sprints](https://sprints.opencraft.com). When the person handling the sprint management is also leading the sprint planning meeting, this responsibility can be delegated to the person taking notes that sprint.

### Cell manager - Epic planning

* Understand the lifecycle of en epic:
    1. Most epics start with a discovery based on a client requirement.
       (For internal projects, the client is OpenCraft).
    1. An epic is created based on the corresponding discovery and starts in the "Prospect"
       column.
    1. The discovery and corresponding estimates are shared with the client, and the epic is
       moved to "Offer / Waiting on client".
    1. If the client accepts the work, the epic is moved to "Accepted".
    1. Once actual development starts, the epic is moved to "In Development".
    1. Once the client's requirements are met, and the client has access to the
       completed work, the epic can be moved to "Delivered to client / QA".
    1. The epic should be moved back from "Delivered to client/ QA" to "In
       Development" if the client requests additional work or modifications
       that need development.
    1. When all the work in the epic is complete (for instance if all upstream PRs
       have been reviewed and merged) the epic can be moved to "Done".
* Recurring epics are generally not based on a project or discovery, but are used
  to track work for different cell roles.
* Each cell has its own Epics board and epics, and is responsible for ensuring
  that the projects those epics represent are being properly delivered based on
  the above lifecycle.
* Every sprint, during the first week, you should evaluate the changes in status
  of epics over the past sprint. This will involve ensuring that:
    * Each epic has an epic update.
    * Delivered epics are moved to "Delivered to client / QA".
    * Completed epics are moved to "Done".
    * Blocked epics are moved to "Offer / Waiting on client".
* Generally, moving an epic from "Prospect" to "Offer / Waiting on client" or to
  "Accepted" isn't controversial. As described above, there are clear steps
  that trigger each of those transitions. In case of any doubt about the correct status
  for an epic leave a comment on the epic.
* Every sprint, compare the Epics board for your cell
  ([Bebop](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=28&useStoredSettings=true),
  [Serenity](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=27&useStoredSettings=true))
  with the corresponding sheet(s) of the
  [epic planning spreadsheet](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit).
  Ensure delivery and bugfix deadlines for individual epics are on target.
  Comment directly on the spreadsheet.
* When an epic is completed, make sure that it is correctly reflected in the
  "Time / Estimates" sheet of the Epic Planning spreadsheet.
* Maintain a count of the amount of time required to complete the accepted
  budgets over the next months in the epic planning spreadsheet
  for your cell. This is used to inform recruitment needs.
* Keep a look out for completed discoveries. If a discovery is generic and the
  estimates it produced can be reused for further discoveries and estimates
  down the line, add it to the
  [Price list](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=1171479307)
  sheet of the epic planning spreadsheet.
* Keep a look out for newcomers and core members joining or leaving the team,
  and add their details to the epic planning spreadsheet.
    * For newcomers this includes adding onboarding time to the onboarding section
      and specifying (or updating) their availability in the availability section.
      *Only make these updates when the corresponding information is fully public,
      i.e., the newcomer should know whether they have been accepted or not
      by the time these updates are made.*
    * For core team members leaving the team, make sure to reset their availability
      for the remaining months of the year.
* Once per sprint, during the first week, post an epic update in the "Epic
  Planning - <CellName>" epic with the following checklist:

```text
h4. Epic planning update (Sprint ???)

* ( ) Make sure all epics have epic updates.
* ( ) If an epic's status has changed make sure it has the correct status on the Epics board. Most importantly:
** ( ) If an epic has been completed, move it to "Done".
** ( ) If an epic has become permanently blocked on the client, move it to "Offer / Waiting on client".
* ( ) Compare the Epics board with the epic planning spreadsheet.
* ( ) Add/Update details in the "Time / Estimates" sheet of the epic planning spreadsheet:
** ( ) For [new epics|https://tasks.opencraft.com/browse/SE-1615?jql=issuetype = Epic AND status in (Backlog, Offer, Accepted, "In development")] (i.e., epics with a status of "Prospect", "Offer / Waiting on client", "Accepted", or "In development").
** ( ) For [completed epics|https://tasks.opencraft.com/browse/SE-999?jql=issuetype = Epic AND status in (Done, Archived)] (i.e., epics with a status of "Done" or "Archived").
** Note that you can filter the list of new/completed epics down to epics from your cell via the "Project" filter.
* ( ) For in-progress epics:
** ( ) Evaluate the amount of time required to complete the accepted budgets over the next months and update the epic planning spreadsheet.
** ( ) Ensure delivery and bugfix deadlines of individual epics are on target (or are being actively discussed on the epics). Comment directly on the epic planning spreadsheet, pinging epic owners as necessary.
** ( ) Ensure the projects from your cell's clients are being properly delivered using the epic management process.
* ( ) For completed discoveries, see if any estimates can be useful more broadly and add them to the "Price list" sheet of the epic planning spreadsheet.
* ( ) Check the calendar and/or the [Looking for mentors|https://forum.opencraft.com/t/looking-for-mentors/131] thread for people joining/leaving the team in the next couple months and update the epic planning spreadsheet as necessary:
** ( ) For people joining the team, add availability and onboarding hours for the coming months.
** ( ) For people leaving the team, update their availability for the coming months. If prompted by your cell's sprint manager, help find new owners for clients and epics belonging to the people leaving.
* ( ) Check the calendar for vacations coming up in the next couple months. If someone will be away for a total of 1 week (or longer), [post a comment mentioning their availability|https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533] in the epic planning spreadsheet.
* ( ) Check the descriptions of the Hosted Sites Maintenance and Small Projects & Customizations epics (SE-1690, SE-1693) and update the list of clients for your cell, following the existing format.
** ( ) Add info about new clients that we recently on-boarded. (Make sure to skip clients that haven't moved past the prospect stage.)
** ( ) Remove info about clients that we no longer work with. (Make sure they have been off-boarded completely before doing this.)
* ( ) Adjust client budgets for sustainability dashboard as necessary.

h4. Notes

...
```

### Cell manager - Sustainability of the cell

* Each cell is meant to be a sustainable entity by itself: its members are the closest to
  most of the work that impacts its sustainability: the successful estimation and delivery of
  each client project.
* Some of the budgets for internal/non-billed accounts are also decentralized to individual cells.
  See [cell budgets](cell_budgets.md).
* The sustainability role is responsible for ensuring the cell keeps the budgets it is responsible
  for in order.

### Cell manager - Prioritization of work

* The cell being the closest to the client needs, and knowing best the work to be performed,
  it's positioned the best to prioritize its own backlog accordingly. To do so, it must
  prioritize in the following order (decreasing priority):
    1. Client work to ensure that the client needs are met, and that they are happy with
       our work.
    1. [Newcomer-friendly tasks](task_workflows.md#newcomer-friendly-tasks) should be ranked high enough in the list of tasks to
       allocate time for core team member review/mentoring. The earlier we can get newcomers up to speed, the better
       the workload will be for everyone.
    1. Internal projects flagged by the management as priorities.
    1. Additional work the cell finds useful to its or OpenCraft's function (or the Open
       edX community's), as long as the cell remains sustainable in proportion of
       billable hours. The cell defines and prioritizes the additional internal work,
       without a specific monthly budget cap (epic time budgets remain a good practice to
       follow, but the amount set doesn't require approval from management).

Note: It's important to keep in mind that to remain capable of leading initiatives at the company
level, not just at the cell level, hierarchy can retain an important role in prioritization.
Anyone in a cell can propose or prioritize a company-wide initiative, provided they get approval
and buy-in from people it would affect. It would escalate to management if there's a conflict or
disagreement about that, for example about how priorities relate to each other.

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=849132736

## Client owner

* Client owners manage their relationship with their clients. All the epics and tasks from a
  given client are done within a single cell, the one the client owner belongs to, but see
  [cross-cell collaboration](organization.md#cross-cell-collaboration) for some nuances around
  task reviews.
* Initial contact with prospects, estimations work. Note that a portion of Gabriel's time is
  assigned to each cell to do most of the initial contact and quote work with prospects.

## Code reviewer

As a **code reviewer**:

* I will give prompt, thoughtful, thorough, constructive code reviews.
* (When reviewing OpenCraft work): I will expect that the PR author has done everything outlined in the
  *PR expectations* part of the [pull request process](pull_request_process.md)
  section - if not, I will ask them to fix that before I start the review.
* (When reviewing non-OpenCraft work): If I get pinged to review a PR, I will
  respond to it within 24 hours. If it is for a ticket in the current sprint, I
  will ensure that I review it within 24 hours, or at least indicate to the
  author when they can expect a review.
* I will aim to minimize review cycles (especially when reviewing non-OpenCraft
  PRs) by leaving as complete a review as possible. Ideally it should point
  authors to the exact changes needed for their PR to be accepted.
* When reviewing work (or discussing it with the assignee before coding even
  begins), I will make sure that the assignee is not introducing code drift,
  i.e. that everything which could be upstreamed is (planned to be) upstreamed
  as part of the work.
* I will always read through the code diff, considering things like:
    * Is the code easily understandable by humans?
    * Is the code of high quality?
    * Are all relevant coding standards followed?
    * Are all UX / a11y / i18n considerations addressed?
    * Is this introducing tech debt?
    * Is the new code well covered by tests?
* I will always test the code manually, either locally or on a sandbox, unless there is no
  reasonable way to do so.
* I will always check if any updates to the software's documentation are required,
  and either ask the author to update the docs, or ensure the relevant documentation team is pinged.
* If there is any part of the code that I am not confident in reviewing,
  I will indicate that in my comments.
* If there is any part of the code or PR that I particularly like, I will say so - we want to reinforce
  good practice as well as flagging issues.
* I will
  [set up the following template as a "Saved Reply" in GitHub](https://github.com/settings/replies)
  and use it for the "stamp of approval" on all PRs I review:

```markdown
:+1:

- [ ] I tested this: (describe what you tested)
- [ ] I read through the code
- [ ] I checked for accessibility issues
- [ ] Includes documentation
- [ ] I made sure any change in configuration variables is reflected in the corresponding client's `configuration-secure` repository.
```

  Here is a screenshot showing how to conveniently access this template
  when completing a code review using the dropdown button in the top right
  of the review panel (on the "Conversation" tab only):

  ![Review template](images/review-template.png)

* If I'm the assigned reviewer for someone who is new to OpenCraft,
  I will reserve extra time to provide additional mentoring and support,
  and I will be especially responsive to questions.
  I will also provide feedback to the newcomer which will assist them during their trial period, and raise any major
  issues with their mentor.
* If the assignee is clearly behind the schedule and doesn't respond at all to pings on the ticket or Mattermost within
  48 hours, the code reviewer should determine whether this ticket is urgent. If it is, ask the firefighter for help to
  reduce the risk of spillover.

## Newcomer

If I'm **new to the team**, I will:

* Not commit to more than [3 story points](task_workflows.md#general-tasks) of tasks during my first week, or 5 points
  my second week (so no more than a total of 8 points for the first sprint).  It's really easy to over-commit at first,
  so keep your commitments manageable. You can always take on more work later in the sprint if you finish early.
* Discuss general issues like time management and sprint planning with my mentor.
* Tag my task reviewer(s) with task-specific questions on the ticket, or if I'm completely blocked, try using
  Mattermost to contact my reviewer, mentor, sprint firefighter, or other people working on the same project.
  Reviewers have time allocated to help during Onboarding, so it's ok to reach out!
* Ask my Reviewer questions **early in the sprint** if there is missing information or if the requirements are unclear.
  [Newcomer-friendly tasks](task_workflows.md#newcomer-friendly-tasks) have a minimum set of information that should be included in the
  task description, but if this isn't complete, ask the task creator and/or your reviewer to do it.
* Provide lots of updates to my assigned reviewer/mentor for each task,
  so they can provide timely help.
* Assign myself as [reviewer](#code-reviewer) for core team member tasks to learn about the various projects and systems
  that we support.
* Log time spent "onboarding" (reading documentation, learning new systems, setting up devstacks) to my onboarding task.
  Time spent completing the work described in a task can be logged to the task itself, but please take care not to
  exceed the estimated time without **approval from the owner of the parent epic**.  Some leeway will likely exist
  within the epic budget, but it's best to check to be sure.

Also, it is not compulsory to log X hours per week as stated in the contract -- our contracts give an estimated amount
of time expected per week, but the actual work required for each sprint can vary due to many factors.

See the [Process: Onboarding & Evaluation](onboarding.md) page for more advice on the onboarding, the evaluation
process and criteria.

### Onboarding epic ownership

As a newcomers, I am the owner of my onboarding project, and will:

* Not use more than 120 hours on my onboarding epic or deplete it before the end of the 6-month period it is intended for. It is normal to use a
  bit more than the suggested weekly cap of 5h during the 2-4 weeks, but as a rule of thumb, one should not spend more than half of that budget
  in the first month.

  The epic and the budget also includes my onboarding ticket, my mentor's mentoring ticket
  and any other tickets created specifically for my onboarding like the ones for my screening review and the end-of-trial review.
  The budget here is shared and the actual available time is less than the total of 120 hours. For example, if the screening review requires
  2 hours and the end of trial review requires 2 hours for each core team member and there are 6 of them, 14 hours (2 + 6*2) is required for
  those activities. That would reduce the actual budget available to 106 hours (120 - 14).

  In the onboarding epic task, there is a `Summary Panel` section on the right side, which shows various details about the budget,
  logged and available time etc. See the below screenshot for an example.

  ![Summary Panel](./images/epic_summary_panel.png)

  On hovering over the time numbers below the progress bar, a tooltip explaining what those numbers are, shows up.

* Treat my onboarding epic as a proper epic and manage it like an [epic owner](#epic-owner). I will create tasks under this epic
  for distinct pieces of work, set estimates and schedule them with the help of my mentor. This will help us to track if a particular
  onboarding/learning task is taking too much time. Since the onboarding budget is limited, all the learning time I log on the onboarding epic
  will be related to the tasks that I work on.

* Log my time with detailed descriptions about the work performed.  For example:

    "Reviewed task and asked questions on ticket, requested repo access, started setting up devstack."

    Or as an example where logging "onboarding" time spent on another task on my onboarding ticket:

    "Onboarding for SE-123: read XBlock Tutorial and configured XBlocks in my devstack"

## Mentor

* I will make sure to inform the team I'll mentor a newcomer before their first day.
* I will familiarize myself with the current [onboarding & evaluation process](onboarding.md), so I can
  answer questions and help the newcomer through the process.
* I will allocate sufficient time in my sprint, especially at the beginning, to assist and mentor the newcomer.
* Before the newcomer's first day, I'll assign him/her a small, newcomer-friendly task for the first week as part
  of the screening process.
* I will help the new member to find tasks that are suitable for someone starting up,
  especially in the first sprint and in the absence of [Newcomer-friendly tasks](task_workflows.md#newcomer-friendly-tasks).
* During the trial period, I will help the newcomer select tasks to showcase their skills and learning abilities, with
  an eye to assigning tasks of increasing difficulty as the trial period progresses.
* If newcomer-friendly tasks are not available in the current sprint or backlog, I will work with the epic owners to split out newcomer tasks from
  other work.
* Alternatively, I can create one from the [INCR project](https://openedx.atlassian.net/projects/INCR/issues/INCR-4?filter=allopenissues) (preferable)
  or [Byte-Sized Bugs](https://openedx.atlassian.net/browse/TNL-5801?filter=12810) upstream boards.
* If no newcomer tasks can be created, I may assign a non-newcomer task. In these cases, I will consider
  factors such as (i) the circumstances of the task, (ii) the trust in the newcomer and (iii) the level of access
  needed for deployment. (Note: any additional access granted needs to be documented in the
  [Access Doc](https://docs.google.com/document/d/1uddEPbq4Phwbjkj3l7SmdO9zZ38QVQrUmGaX4AC3sHk/edit))
* I may assign very small discoveries, which do not result in a standalone epic.
  (See [Newcomer-friendly tasks discussion](https://forum.opencraft.com/t/newcomer-friendly-tasks/318))
* If a task requires deployment, one option is that the reviewer does the deployment,
  but lets the newcomer know what they’re doing at each step, to help them learn for the future.
* I will schedule an initial 121 meeting with the newcomer, and a recurring meeting every week.
  These can be spaced out later on if the meetings become less useful. Refer to
  [the mentor checklists page](mentor_checklists.md) for the topics to go through.
* For the first 121 with the newcomer, I'll prepare a few interview questions to help in the screening process.
* I will participate on the screening and developer review tasks, taking into account these
  [evaluation criteria](onboarding.md#evaluation-criteria).
* I will evaluate the newcomer's usage of the onboarding budget and raise any issues with Xavier. I'll also help the newcomer with
  creating, estimating and scheduling learning tasks in the onboarding epic, tracking them and with
  precise time tracking of his/her tasks if the work logs show too many approximate (i.e. rounded) values.
* I will also explain to the newcomer, the guidelines to manage the onboarding epic and its budget like
  any other epic that we work on and point them to the relevant items in the [newcomer](#newcomer) section.


## Firefighter

There are two firefighters per cell for each sprint and they are designated as Firefighter 1 and 2.
Besides minor differences who leads which sprint meeting, these roles have exactly the same
responsibilities.

As a **sprint firefighter**:

* I will keep at least 15 hours of time for sprint firefighter duties as
  described below, and will proactively pursue those duties.
* I will assign tasks for the rest of my sprint hours normally, but I will also ensure a few additional tasks
  are left either in the "stretch goals" or the following sprint during sprint planning, in case there
  isn't enough firefighting work to fill my hours. I will only pull these tasks into the current sprint
  if I am confident that I will have time to finish them in addition to the firefighting. These tasks
  should be assigned to me and have a reviewer, to be ready to pull in.
* I will not record any time on the Sprint Firefighter
  task directly (always use a dedicated client story/bug/epic ticket).
* I will be subscribed to the
  [help](http://mail.plebia.org/cgi-bin/mailman/listinfo/help)
  and
  [urgent](http://mail.plebia.org/cgi-bin/mailman/listinfo/urgent)
  mailing lists (be sure to filter them to a separate folder
  to look at them only when you need to - but also make sure
  that if any such emails also explicitly include you in the To/CC,
  then they will arrive in your inbox).
* I will [add myself to the pager rotation](https://courses.opencraft.com/courses/course-v1:OpenCraft+onboarding+course/jump_to/block-v1:OpenCraft+onboarding+course+type@vertical+block@first_steps_as_a_core_team_member_firefighting_pager_setup), at least during my work hours the weeks
  I am firefighter, plus any additional time I optionally want to help cover. 
* I will work on the following, listed by decreasing priority:
    * Handle emergencies from other team members
      (reported by team members directly to me or to the other sprint
      firefighter, with Braden arbitrating priorities)
    * Handle emergencies reported by clients from the cell--these will come in through the
      help@opencraft.com mailing list, which everyone receives.
      Firefighters triage and prioritize these issues, asking other cell members for advice
      as needed, and escalating to Braden/Xavier if everything else fails.
    * Handle critical bugs reported by QA
    * Deploying hotfixes/security fixes on client instances
    * Provide reviews for tasks that are missing a reviewer.
    * Complete any personal spillover from the previous sprint
    * Work on client requests that can't wait until the next sprint, in particular in
      the first week of a sprint.
    * Help ensuring a clean sprint by helping other team members, in particular in the
      second week of a sprint.
    * Being available on the Open edX Slack (#general), answering questions and
      responding to emergencies reported there (log time on OC-1017)
    * Watch over sandboxes (ie keep an eye on the instance manager
      on a regular basis, few times a day, to ensure they build correctly, and debugging if needed)

  **Dependency upgrades for security issues**

  GitHub sends the security alerts by scanning (by default, all public repositories are scanned; for private repositories, the admins have to explicitly permit the scanning) the repository dependencies, and based on the severity levels:

  * `Critical and High level` severity vulnerabilities: As there could be a risk of compromise or significant downtime for users, these vulnerabilities must be patched as soon as possible. The FFs should create the tasks for applying the patch and start working on them right away.
  * `Medium level` severity vulnerabilities: Report them and create tasks for patching them so that team is aware of them. These tasks can be scheduled for the next sprint and if any of the FFs have time, they can work ahead on these tasks.
  * `Low level` severity vulnerabilities: These vulnerabilities should be reported and tasks for patching them created with a lower priority. These tasks can be scheduled for a future sprint and prioritized by the epic owner as appropriate.
  * `Undefined or unclear` severity: Vulnerabilities or security fixes whose severity is unknown should be reported and discussed with team before we can take any action.
  * `Dependabot PRs`: Dependabot PRs for bumping the version of vulnerable dependencies or security fixes should be handled by the FFs based on the process mentioned above for various severity levels.

  The severity level mentioned in a vulnerability report may not always match our own assessment (`critical issues` in a disabled feature may not be `critical`). Hence the processes around categorization of the security vulnerabilities and the assessment of their severity levels need to be expanded and improved over time.

* Work on additional tasks from the backlog if additional time is available
* Well before the sprint planning meeting at the start of a new sprint (every
  second Monday), I will remind the epic owners to get every ticket in the upcoming sprint
  "green" (has an assignee, reviewer, and points) by pinging people as needed,
  and I will help make sure all tickets are green before the start of the sprint planning meeting.
  If my timezone makes this challenging, I will coordinate with Firefighter 2 to finish up on Monday.
  The time spent getting a ticket to green will be logged on the ticket.
* If I am Firefighter 1, I will lead the sprint planning meeting at the start of
  the sprint. If I am Firefighter 2, I will take notes during this meeting. For the
  mid-sprint meeting these roles are reversed, Firefighter 2 will lead the meeting
  and Firefighter 1 will take notes.
* On Friday or Monday well before the mid-sprint meeting, I will do a sprint checkin
  on all "backlog" and "In progress" tickets (this means to post a comment
  on each ticket to ask how the assignee is doing and if they need help,
  and/or asking them to update the status of the ticket if it is not clear).

## Community Relations

### Official forum support

 * Rotating role, assigned to two cell members each sprint.
 * Keep 1h/week for providing support on the [Open edX forum](https://discuss.openedx.org/) and Open edX [Slack team](http://openedx.slack.com/), monitoring for technical questions and answering them.
   Generally favor helping one person more fully, over having many smaller contributions to more threads.
   It should give a feeling of what a support task with a 1h timebox gives.
   If it takes less than 1h to solve, you can help someone else with another problem with the remaining time.
   But otherwise spend that time on a single question/issue - one that you think you can contribute something useful to many,
   or that you're especially suited for. If you run out of time, simply stop answering - someone else from the community can take over.
   If you think it would be helpful to go beyond 1h in some cases, check a timebox extension with Xavier.
 * Time must be logged on a ticket dedicated for the role each week - see [SE-1567](https://tasks.opencraft.com/browse/SE-1567) or [BB-1692](https://tasks.opencraft.com/browse/BB-1692) for examples.

### Official forum moderator

* As a moderator, edX is looking for help with:
    * Welcome new posters
    * Help people use the forums well, including:
        * Choose the right categories, and move/split/concatenate threads when needed.
        * Provide enough information about their situation - we can help them to refine their questions, asking them questions to make their requests more precise
        * Direct them to existing resources
    * Review posts flagged automatically by Discourse (for example, someone finishing a post too quickly after creating their account)
    * Set the right tone for discussions and be model Open edX citizens
* This isn't about knowing the answers to questions - though it never hurts, and you can also provide support at the same time if it makes sense. But Ned defines it as "being involved with the forum in a way that makes it productive and useful for people."
* The budget is 1h/week, in total between all the forum moderators

### OSPR Liaison

* Keep 1h/week available to prioritize Open OSPRs for review by edX.
* Maintain the prioritized list in a document shared with the OSCM team:
  [OSPR priorities from OpenCraft](https://docs.google.com/document/d/1FigwxhOwBpt7B-d5IBBmkFIgBLZQDdgsX6LFWR_90Ew/edit?usp=sharing)
* Send the top 3 on the list to the edX OSCM team (usually Natalia) once a week.
* Log time on [SE-1631](https://tasks.opencraft.com/browse/SE-1631).
* Keeep 0.5hrs/week available to handle any PR sandbox requests by edX.
* Someone (likely [Ned](https://github.com/nedbat) or [Natalia](https://github.com/natabene)) will ping you on a PR targeting an older release branch (currently limited to Hawthorn and later).
* Make sure the PR has a reviewer assigned. If not, request that one be assigned before continuing with the next step.
* Create a new sandbox for the PR, and link the sandbox URLs to the PR.
  * To do this you can use the ``setup_pr_sandbox`` Ocim management command and provide it the URL of the edx-platform PR.
* In case there isn't sufficient time, create a ticket and ping the firefighter to do the actual sandbox creation steps.
* You can give SSH access to the reviewer by adding their GitHub ID to ``COMMON_USER_INFO`` in the PR sandbox's config:
  ```yaml
  COMMON_USER_INFO:
  - github: true
    name: github_id
    type: admin
  ```
* You can ask the PR author to add the standard setting section to the PR description with the above config. Otherwise you may need to add this back each time the sandbox is updated from the PR.

## Ops reviewer

1. Receive ops@opencraft.com and the pager alerts - check alerts/emails to ensure the alerts
   have been properly handled by the other recipients and nothing has slipped through.
1. Be a backup on the pager (alerts sent first to the other(s) recipient(s), but escalated if not
   acknowledged within 30 minutes).
1. If none of the other recipients are around and an issue is left unsolved, and it either is sent
   to urgent@, or was sent more than 12h ago to ops@opencraft.com and needs to handled
   quickly, warn the sprint firefighter about it: create a ticket about the issue and assign it.

## Discovery duty assignee

As the **week's discovery duty assignee**:

* I will make sure I am the right person to take on a specific discovery, if I feel I am not
  I will exchange work with other people from the sprint, to get someone else
  to do it, while I help them with their tasks. It is very important to be able to estimate a
  task well, we should make a conscious effort to assign the task to the person who can best
  handle it, if possible (if the person is available and willing to do the work).
* I will keep at least 5 hours of my time to do discovery work on any small leads/client
  requests that come up during the week. I may still plan some tasks to pull into the sprint
  if no fires pop up. Before pulling tasks in the sprint, I will use these hours to help others finishing
  their sprint commitments.
* If I do some discovery tasks, I will ping the sprint firefighters to review the result.
* As always, if I do the discovery, I understand that I would be expected to complete the epic
  work later on if the client accepts it.
  (To avoid people being forced to commit to being the owner of big epics, any
  huge discovery stories should always be scheduled into future sprints rather
  than directly assigned to the discovery duty assignee.)
* I will ensure an additional task is left either in the "stretch goals" or the following sprint
  during sprint planning, in case there isn't enough discovery duty work to fill my hours. I will
  only pull this task into the current sprint if I am confident that I will have time to finish it
  in addition to the discovery duty. This task should be assigned to me and have a reviewer, to
  be ready to pull in.

## Specialist roles

Specialists are people in the cell with more context and understanding about a certain topic, such as DevOps, Ocim, or Analytics/Insights.
The goal of having this role is to allow cells to self-sustain and run its own projects without being blocked on context from people from another cell.

While specialists have priority on taking tasks related to its specialty, there's no restriction on other team members and taking tasks is encouraged to spread knowledge around the cell.
Specialists should always be at least the 2nd reviewer so they are in a position to track the ticket and provide help.

Specialists are not only responsible for handling tasks within the cell, but also for coordinating with other specialists to discuss and schedule improvements when necessary.

The next sections cover specific tasks to be done by each type of specialist:

### DevOps Specialist

The specialist should always be involved in some way in the devops tasks of the cell, if not as the assignee, then as a reviewer or 2nd reviewer.

 They also have priority to take DevOps tasks as assignee as much as they want, they don't have to take everything (neither should they try), but they should be able to use the context of tasks, especially client tasks, to implement useful work for the infrastructure as a whole.

 Client owners are still responsible for handling DevOps tasks of their client's instance. The specialist should help, advise and review the tasks, while the epic/client owner creates the tasks and handles getting them assigned.

Responsibilities within its cell:

1. Assign, take or review DevOps related tasks.
1. Support team members on DevOps related tasks, provide information about deployment mechanisms, sidecar services, and any particular detail that might affect the outcome of the task.

Responsibilities shared between DevOps specialists across cells:

1. Create tickets to maintain and update common server infrastructure and its related deployment mechanisms (MySQL, MongoDB, Consul, Vault, Prometheus, etc).
1. Create discovery tickets and epics to improve infrastructure, taking into account the current epic schedule and throughput available for non billable DevOps work.

### Technical Security Manager

* Responsible for the **technical security** of the work we do, our platform and OpenCraft as
  a whole.
* Responsible for **[OpenCraft's Security policy](http://opencraft.com/doc/handbook/security_policy/)**.
* **Offboarding** of team members who have left.

## Epic owner

As an **epic owner**:

* I will make sure the epic has tasks with time estimates and a global budget. A discovery will likely be required for this - read the [estimates](how_to_do_estimates.md) section and follow the steps listed there.
* I will ensure the "Timeline" section of the description and the epic due date
  are kept up to date. The "Timeline" section should include a list of
  milestones and deadlines for each (these milestones could be as simple as
  "check in with client"). The due date should be set to the closest
  deadline, and must be revised once each deadline is past/updated.
* I will keep clients updated on the status of tickets
  (for clients with long term monthly contracts, this means updating
  their JIRA or Trello boards as work progresses).
* I will attend meetings with the client about the project as required,
  e.g. weekly scrum meetings.
  (We try to limit these sort of meetings to once a week per client at the most.)
* I will create tickets on OpenCraft's JIRA board for upcoming work,
  and place them into the appropriate upcoming sprints
  so that we can get the work done well ahead of applicable deadlines.
    * If a ticket without point estimates is scheduled for the upcoming sprint,
      and an estimation session is open, I will make sure to add it to that session,
      so everyone on the team can help estimate it.
* I will aim to split out any suitable newcomer-friendly work into separate tasks, flag them as
  [Newcomer-friendly tasks](task_workflows.md#newcomer-friendly-tasks) in the Backlog, and ensure they contain the required information.
  To guard the epic budget, "onboarding" time can be logged against the newcomer's epic.
* If applicable, I will create corresponding tickets on clients' JIRA or Trello boards
  and link to those tickets from the description of internal tickets as necessary.
  (In many cases, a single internal ticket from OpenCraft's JIRA
  will correspond to a single ticket from a client's JIRA or Trello board.
  But there are also cases where a single internal ticket might map to
  multiple external tickets or vice versa.)
* I will set the "Original Estimate" of each ticket to the number of hours
  listed in the discovery document. If a ticket covers a larger story
  from the discovery document only partially, I will set its "Original Estimate"
  to an appropriate fraction of the number of hours listed in the discovery document.
  For any ticket that does not directly correspond to a story from the discovery document,
  I will set the "Original Estimate" based on the amount of time that I think
  will be required to complete the work, taking into account the number of hours
  that remain in the epic's budget at that point in time.
* On Thursday in the second week of a sprint, I will post an epic update
  that answers these questions:

```text
h5. Epic Update (Sprint ???)

h6. What's the status of the hours budget for this epic?

We've used __ hours of the the __ hour budget.

h6. What's left to do? (Is it on track to be done before the deadline and within the hours budget?)

(describe)

h6. Are the deadlines in the "Timeline" section of this epic's description correct?

(/) or (x)

h6. Are all upcoming stories that we should work on in the next week or two created and in the correct upcoming sprint?

(/) or (x)

h6. Is the status of each story in the epic correct?

(/) or (x)

h6. Other Notes

...
```

  *Other Notes* is for any concerns, questions, upcoming vacations, or anything else
  that you think is worth mentioning because it could affect planning
  of the next sprint or two.

* On Thursday or Friday in the final week of each sprint, I will assign
  upcoming stories from this epic that need to be done next sprint
  to myself - or I will ask others from my cell to be either the assignee or the reviewer.
  (If the epic owner is away, the epic reviewer is responsible for this.)

## Client owner

For each client, we have a single person who is designated as the owner for that client, called
the "client owner." The client owner is responsible for handling most communications with that
client. The client owner is the person who is assigned to the epic or task with the name
"\<Client name> [General] Support", such as [OC-2340](https://tasks.opencraft.com/browse/OC-2340)
or [OC-2910](https://tasks.opencraft.com/browse/OC-2910). If nobody is assigned to such an epic,
or it doesn't exist, the epic owner of the current year's
[Small Projects & Customizations epic](https://tasks.opencraft.com/browse/OC-4105) is the default
client owner.

In general, regardless of which email address the client sent their question to, the client owner
should be the one who replies, though they may hand off any conversation to anyone on the team as
needed (such as their cell's firefighter). If someone else received the email, please "Reply All" to pass the
message on to the client owner for them to reply (add the client owner to the "To" field, and add
"OpenCraft <help@opencraft.com>" to the CC field, and say something like "Passing this on
to (client owner name)").

There are a few exceptions:

* If it's an urgent problem, whoever sees it first should reply and CC the firefighters,
  help@opencraft.com, and the client owner. One of the firefighters should respond.
* If it's a general issue/question:
    * If the email is related to a specific project/epic with a different epic owner, that epic
      owner can reply directly (make sure the client owner and help@opencraft.com are CC'd).
    * If the client owner is away, their designated backup person (usually the reviewer on the
      "Support" epic/ticket for that client) should take over this role. (Or the firefighters
      if no backup person was planned.)

We have a few clients with monthly development budgets. For those clients, the assigned client owner
is also responsible for reviewing the monthly budget before and during each sprint planning meeting,
to ensure that we are not going too far above or below the budget. They should refer to the
[Sprints](https://sprints.opencraft.com)
which can monitor and project the status of each budget as of the end of the upcoming sprint.

When we get a new client, or when the client owner changes (or we start working with a new
person coordinating things on the client's side), we send them
[a welcome email](https://docs.google.com/document/d/1TFBgmq5QA318IfVLBbg9owuqyVl7e4UX5ZWsFQl-u6o/edit)
explaining who the client owner is and how to contact us.

## Product specialist

As a **product specialist**:

* I will ensure clear and prompt communication with prospects, leads and clients.
* I will regularly look out for business opportunities. This includes
  consulting public tendering platforms, sending cold emails to prospects,
  and attending events where you could meet prospects.
  Strategy and time spent on these tasks should be discussed with the cell sponsoring the
  prospect work, and possibly Xavier for key prospects.
* I will send the official OpenCraft offering presentation materials to prospects.
* When meeting, I will describe OpenCraft’s offering, history and philosophy to prospects.
* I will quickly gather prospects’ needs and potential time budget through discussion.
  This enables us to know if and how we can help them.
* I will keep an open mind. Buyers might have needs that we can’t fulfill yet - this might
  be an opportunity and should be discussed with the cell, and possibly Xavier for key
  prospects.
* When a prospect becomes a lead, I will follow-up by providing information,
  drafting quotes, creating epics and issues and documenting in Jira so the team can follow
  what’s happening. I will include the template in the epic description ("Context, Scope,
  Budget, Timeline") and set the time budget on the epic ticket.
* Handle the onboarding of confirmed clients: tell them how to work with us, such as when/how
  to use the urgent@opencraft.com email address, when/how to use help@opencraft.com, who
  their main contact(s) should be, how billing works, etc.
* When managing a project, I will handle communication and coordination
  between the client and the cell working on the project, and assign discovery tasks to
  developers until the client approves the quote.
* Generally, when writing to the clients, I will always make sure there is either contact@
  or help@ CC'd:
    * anything related to accounting/billing/quote should be on contact@
    * and the rest on help@
* I will manage and monitor self-hosting registrations and addressing user issues in Ocim
* Handling billing and accounting for self-hosted users
* Handling contact@ email
* [OpenCraft lead management flowchart](https://docs.google.com/drawings/d/12GXlLwWgb3qBB-iDjHJJegV1qqHJ6Fru6RVjhgtj6IQ/edit?usp=sharing)
* [OpenCraft client onboarding flowchart](https://docs.google.com/drawings/d/15RceotA5oqKNr0gQCR924B9OnbY5xy6E7b2xezaIkyc/edit?usp=sharing)

## Learning designer

* Providing clients with pedagogical advice and strategy
* Providing clients with custom learning experience designs

## CTO

* **Technical Arbiter**
    * Final arbiter for technical decisions
* **Technical Excellence**
    * Ensuring technical & product quality of everything we do
    * Ensuring software we produce is as open as possible and everything that can be contributed upstream is
    * Championing and improving processes (like our code review process) to improve the quality and efficiency of our software development
    * Reviewing code (in addition to the regular code review process), mentoring developers
    * Being aware of the "big picture" to help developers avoid duplicating each other's work
    * Being available on call 24/7 to respond to emergencies, if regular firefighters are not available
* **Epic/Client Owner Coach**
    * Supporting epic and client owners in client meetings, discussions, and in turning requirements into actionable plans/epics
    * Reviewing all discovery stories and estimates
* **Security Manager**
    * Ultimately responsible for security of our software, both proactively improving it and responding to incidents effectively

The CTO isn't part of a cell, but can review and participate in any cell's work as needed.

## Sprint Manager

Tasks shared between sprint managers from all cells:

* Close the current sprint and start new one after sprint meeting.

Tasks to be done individually by each sprint manager:

* Create grooming session on Wednesday before next sprint, and close it before the sprint meeting, allowing time for the assignees to take them and make proper planning.
  * When creating the session, add all the unestimated tickets for next sprint and on stretch goals to it.
  * Add epic owners as Scrum Masters, so that they can add it the tickets they create or pull in after the estimation session was created.
  * Ping everyone that hasn't participated on the estimation session to estimate before closing the session.
* Create and assign Firefighter and Discovery Duty tickets for upcoming sprint.
* Review of sprint deliveries: Move tasks from "Deployed & Delivered" to "Done", after double-checking that all [the criteria for calling it "Done"](task_workflows.md#done) have been met.
* Review of sprint planning & tickets in "waiting for 3rd party" and "long reviews".
* Update the [sprint commitments spreadsheet](https://docs.google.com/spreadsheets/d/1FXjV9GCugQbX9gS7at2LDDAYvw8A8Z_PrRBG0DaFOho/edit) at end of sprint.

## CEO

* **Business development**:
    * Additional relationship with key clients and prospects
    * Support role for the epic owner, in difficult situations or contract negotiations
    * Reviewer of quotes
    * Initiator for opportunities that are strategically important for OpenCraft, either
      because of size or side effects
* **Financials**:
    * Accounting
    * Invoicing (clients & team)
    * Forecasting
* **Admin**:
    * Holidays reviews
    * Partial offboarding (main work done by CTO)
* **Legal**:
    * Relationship with lawyers
    * Contracts drafting and signature (team and clients)
    * Ownership of legal projects
* **Management**:
    * Management of non-technical matters for all cells
    * Management of CTO
    * 121 meetings: continuous rotation of all employees, at 2-3 meetings/week
    * Handle reports of performance issues & process to address it, including firing if needed
* **Hiring**:
    * Sourcing of initial candidates, upon demand by individual cells
    * Contract negotiation with accepted candidates
    * Granting access to new hires & core developers
* **Sprint management**:
    * Occasional second review of sprint deliverables, sprint planning & epics management on any
      cell
* **Strategy**: evolution of the company
* **Ocim**: product management and some code
* **Spreading the word** (mailing lists, conferences, opencraft.com)
